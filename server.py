from vulnerability import cross_file_vulnerability

import os

from flask import request
app = Flask(__name__)

@app.route('/api/<name>')
def api_endpoint(name):
    cross_function_vulnerability(name)
    cross_file_vulnerability(name)

def cross_function_vulnerability(param: str) -> None:
    # THIS IS A TRUE POSITIVE THROUGH A CROSS FUNCTION VULNERABILITY
    os.system(param)

def not_cross_function_vulnerability(param: str) -> None:
    # THIS IS A FALSE POSITIVE BECAUSE THIS FUNCTION IS NOT IN USE
    os.system(param)
