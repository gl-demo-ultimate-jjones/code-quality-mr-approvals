RULE_NAME="Code Quality check"
defects=$(jq length gl-code-quality-report.json)

if ((defects > 0))
then
    echo "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/approval_rules"
    curl -X POST -H "Content-Type: application/x-www-form-urlencoded" -H "PRIVATE-TOKEN: $ACCESS_TOKEN" -d "approvals_required=$CQ_APPROVALS_COUNT&name='$RULE_NAME'&usernames=$CQ_APPROVERS" "https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${CI_MERGE_REQUEST_IID}/approval_rules"
fi
